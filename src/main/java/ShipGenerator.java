import java.util.Random;

public class ShipGenerator implements Runnable {

    private Tunnel tunnel;
    private int size;


    public ShipGenerator(Tunnel tunnel, int size) {
        this.tunnel = tunnel;
        this.size = size;
    }

    @Override
    public void run() {
//в цикле запускает создание кораблей и передачу их в тоннель
    }

    //вспомогательные методы для генерации кораблей.
    private Type getRandomType() {
        Random random = new Random();
        return Type.values()[random.nextInt(Type.values().length)];
    }

    private Size getRandomSize() {
        Random random = new Random();
        return Size.values()[random.nextInt(Size.values().length)];
    }
}

