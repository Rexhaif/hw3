public class PierLoader implements Runnable {
    private Tunnel tunnel;
    private Type shipType;

    public PierLoader(Tunnel tunnel, Type shipType) {
        this.tunnel = tunnel;
        this.shipType = shipType;
    }

    @Override
    public void run() {

        while (true) {

            Ship ship = tunnel.get(shipType);
            //берет из тоннеля корабль соответствующего типа

            while (ship.countCheck()) {
                //загружает на него товар, пока корабль не наполнится
            }
        }
    }
}
